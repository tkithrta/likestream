FROM node:22-bookworm-slim as builder

WORKDIR /app
COPY . .

RUN npm i

FROM gcr.io/distroless/nodejs22-debian12

WORKDIR /app
COPY --from=builder /app .

ENV HOSTS=0.0.0.0
ENTRYPOINT ["/nodejs/bin/node", "index.js"]
