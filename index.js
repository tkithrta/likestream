const crypto = require("crypto");
const fs = require('fs');
const express = require("express");

require("dotenv").config();
let configJson;
if (process.env.CONFIG_JSON) {
  configJson = process.env.CONFIG_JSON;
  if (configJson.startsWith("'")) configJson = configJson.slice(1);
  if (configJson.endsWith("'")) configJson = configJson.slice(0, -1);
} else configJson = fs.readFileSync("data/config.json", 'utf8');
const CONFIG = JSON.parse(configJson);
const ME = [
  '<a href="https://',
  new URL(CONFIG.me).hostname,
  '/" rel="me nofollow noopener noreferrer" target="_blank">',
  "https://",
  new URL(CONFIG.me).hostname,
  "/",
  "</a>",
].join("");
const ALLOWLIST = process.env.ALLOWLIST.split(",");
let privateKeyPem = process.env.PRIVATE_KEY;
privateKeyPem = privateKeyPem.split("\\n").join("\n");
if (privateKeyPem.startsWith('"')) privateKeyPem = privateKeyPem.slice(1);
if (privateKeyPem.endsWith('"')) privateKeyPem = privateKeyPem.slice(0, -1);
const PRIVATE_KEY = privateKeyPem;
const publicKeyPem = crypto.createPublicKey(PRIVATE_KEY).export({ type: "spki", format: "pem" });

const app = express();
app.use(express.static("public"));
app.use(express.json({ type: "*/*" }));
app.set("trust proxy", true);

function uuidv7() {
  const v = new Uint8Array(16);
  crypto.getRandomValues(v);
  const ts = BigInt(Date.now());
  v[0] = Number(ts >> 40n);
  v[1] = Number(ts >> 32n);
  v[2] = Number(ts >> 24n);
  v[3] = Number(ts >> 16n);
  v[4] = Number(ts >> 8n);
  v[5] = Number(ts);
  v[6] = v[6] & 0x0f | 0x70;
  v[8] = v[8] & 0x3f | 0x80;
  return Array.from(v, (b) => b.toString(16).padStart(2, "0")).join("");
}

async function getActivity(username, hostname, req) {
  const t = new Date().toUTCString();
  const sig = crypto
    .createSign("sha256")
    .update(
      [
        `(request-target): get ${new URL(req).pathname}`,
        `host: ${new URL(req).hostname}`,
        `date: ${t}`,
      ].join("\n"),
    )
    .end();
  const b64 = sig.sign(PRIVATE_KEY, "base64");
  const headers = {
    Date: t,
    Signature: [
      `keyId="https://${hostname}/u/${username}#Key"`,
      'algorithm="rsa-sha256"',
      'headers="(request-target) host date"',
      `signature="${b64}"`,
    ].join(),
    Accept: "application/activity+json",
    "Accept-Encoding": "identity",
    "Cache-Control": "no-cache",
    "User-Agent": `Likestream/1.3.0 (+https://${hostname}/)`,
  };
  const res = await fetch(req, { method: "GET", headers });
  const status = res.status;
  console.log(`GET ${req} ${status}`);
  const body = await res.json();
  return body;
}

async function postActivity(username, hostname, req, x) {
  const t = new Date().toUTCString();
  const body = JSON.stringify(x);
  const s256 = crypto.createHash("sha256").update(body).digest("base64");
  const sig = crypto
    .createSign("sha256")
    .update(
      [
        `(request-target): post ${new URL(req).pathname}`,
        `host: ${new URL(req).hostname}`,
        `date: ${t}`,
        `digest: SHA-256=${s256}`,
      ].join("\n"),
    )
    .end();
  const b64 = sig.sign(PRIVATE_KEY, "base64");
  const headers = {
    Date: t,
    Digest: `SHA-256=${s256}`,
    Signature: [
      `keyId="https://${hostname}/u/${username}#Key"`,
      'algorithm="rsa-sha256"',
      'headers="(request-target) host date digest"',
      `signature="${b64}"`,
    ].join(),
    Accept: "application/json",
    "Accept-Encoding": "gzip",
    "Cache-Control": "max-age=0",
    "Content-Type": "application/activity+json",
    "User-Agent": `Likestream/1.3.0 (+https://${hostname}/)`,
  };
  console.log(`POST ${req} ${body}`);
  await fetch(req, { method: "POST", body, headers });
}

async function follow(username, hostname, x) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}`,
    type: "Follow",
    actor: `https://${hostname}/u/${username}`,
    object: x.id,
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function undoFollow(username, hostname, x) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}#Undo`,
    type: "Undo",
    actor: `https://${hostname}/u/${username}`,
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Follow",
      actor: `https://${hostname}/u/${username}`,
      object: x.id,
    },
  };
  await postActivity(username, hostname, x.inbox, body);
}

async function like(username, hostname, x, y) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}`,
    type: "Like",
    actor: `https://${hostname}/u/${username}`,
    object: x.id,
  };
  await postActivity(username, hostname, y.inbox, body);
}

async function undoLike(username, hostname, x, y) {
  const aid = uuidv7();
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/s/${aid}#Undo`,
    type: "Undo",
    actor: `https://${hostname}/u/${username}`,
    object: {
      id: `https://${hostname}/u/${username}/s/${aid}`,
      type: "Like",
      actor: `https://${hostname}/u/${username}`,
      object: x.id,
    },
  };
  await postActivity(username, hostname, y.inbox, body);
}

app.get("/", (_req, res) => {
  res.type("text/html").send("<h1>Likestream</h1><!-- https://gitlab.com/tkithrta/likestream -->");
});

app.get("/about", (_req, res) => {
  res.type("text/html").send("<h1>Likestream</h1><!-- https://gitlab.com/tkithrta/likestream -->");
});

app.get("/u/:username", (req, res) => {
  const username = req.params.username;
  const hostname = new URL(CONFIG.origin).hostname;
  const acceptHeaderField = req.header("Accept");
  let hasType = false;
  if (username.length !== 1) return res.sendStatus(404);
  if (!/^[a-z]$/.test(username)) return res.sendStatus(404);
  if (acceptHeaderField?.includes("application/activity+json")) hasType = true;
  if (acceptHeaderField?.includes("application/ld+json")) hasType = true;
  if (acceptHeaderField?.includes("application/json")) hasType = true;
  if (!hasType) {
    const body = "<h1>Likestream</h1><!-- https://gitlab.com/tkithrta/likestream -->";
    const headers = {
      "Cache-Control": `public, max-age=${Number(CONFIG.ttl) || 0}, must-revalidate`,
      Vary: "Accept, Accept-Encoding",
    };
    return res.set(headers).type("text/html").send(body);
  }
  const body = {
    "@context": [
      "https://www.w3.org/ns/activitystreams",
      "https://w3id.org/security/v1",
      {
        schema: "https://schema.org/",
        PropertyValue: "schema:PropertyValue",
        value: "schema:value",
        Key: "sec:Key",
      },
    ],
    id: `https://${hostname}/u/${username}`,
    type: "Person",
    inbox: `https://${hostname}/u/${username}/inbox`,
    outbox: `https://${hostname}/u/${username}/outbox`,
    following: `https://${hostname}/u/${username}/following`,
    followers: `https://${hostname}/u/${username}/followers`,
    preferredUsername: username,
    name: username.toUpperCase(),
    summary: "<p>1.3.0</p>",
    url: `https://${hostname}/u/${username}`,
    endpoints: { sharedInbox: `https://${hostname}/u/${username}/inbox` },
    attachment: [
      {
        type: "PropertyValue",
        name: "me",
        value: ME,
      },
    ],
    icon: {
      type: "Image",
      mediaType: "image/png",
      url: `https://${hostname}/public/u.png`,
    },
    image: {
      type: "Image",
      mediaType: "image/png",
      url: `https://${hostname}/public/s.png`,
    },
    publicKey: {
      id: `https://${hostname}/u/${username}#Key`,
      type: "Key",
      owner: `https://${hostname}/u/${username}`,
      publicKeyPem,
    },
  };
  const headers = {
    "Cache-Control": `public, max-age=${Number(CONFIG.ttl) || 0}, must-revalidate`,
    Vary: "Accept, Accept-Encoding",
  };
  res.set(headers).type("application/activity+json").json(body);
});

app.get("/u/:username/inbox", async (_req, res) => res.sendStatus(405));
app.post("/u/:username/inbox", async (req, res) => {
  try {
    const username = req.params.username;
    const hostname = new URL(CONFIG.origin).hostname;
    const contentTypeHeaderField = req.header("Content-Type");
    let hasType = false;
    let isOk = false;
    const y = req.body;
    let t = y.type ?? "";
    const aid = y.id ?? "";
    const atype = y.type ?? "";
    if (aid.length > 1024 || atype.length > 64) return res.sendStatus(400);
    console.log(`INBOX ${aid} ${atype}`);
    if (username.length !== 1) return res.sendStatus(404);
    if (!/^[a-z]$/.test(username)) return res.sendStatus(404);
    if (contentTypeHeaderField?.includes("application/activity+json")) hasType = true;
    if (contentTypeHeaderField?.includes("application/ld+json")) hasType = true;
    if (contentTypeHeaderField?.includes("application/json")) hasType = true;
    if (!hasType) return res.sendStatus(400);
    if (!req.header("Digest") || !req.header("Signature")) return res.sendStatus(400);
    if (t === "Follow" || t === "Undo") return res.status(200).end();
    if (t === "Accept" || t === "Reject" || t === "Add") return res.status(200).end();
    if (t === "Remove" || t === "Like" || t === "Announce") return res.status(200).end();
    if (t === "Update" || t === "Delete") return res.status(200).end();
    for (let i = 0; i < ALLOWLIST.length; i++) {
      if (new URL(y.actor || "about:blank").href !== new URL(ALLOWLIST[i] || "about:blank").href) continue;
      const x = await getActivity(username, hostname, y.actor);
      if (!x || new URL(x.inbox || "about:blank").origin !== new URL(ALLOWLIST[i] || "about:blank").origin) continue;
      if (t === "Create") {
        const z = y.object ?? {};
        t = z.type ?? "";
        if (t === "Note") {
          await like(username, hostname, y, x);
          isOk = true;
        }
      }
    }
    if (isOk) return res.status(200).end();
    res.sendStatus(500);
  } catch (_err) {
    res.sendStatus(500);
  }
});

app.post("/u/:username/outbox", (_req, res) => res.sendStatus(405));
app.get("/u/:username/outbox", (req, res) => {
  const username = req.params.username;
  const hostname = new URL(CONFIG.origin).hostname;
  if (username.length !== 1) return res.sendStatus(404);
  if (!/^[a-z]$/.test(username)) return res.sendStatus(404);
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/outbox`,
    type: "OrderedCollection",
    totalItems: 0,
  };
  res.type("application/activity+json").json(body);
});

app.get("/u/:username/following", (req, res) => {
  const username = req.params.username;
  const hostname = new URL(CONFIG.origin).hostname;
  if (username.length !== 1) return res.sendStatus(404);
  if (!/^[a-z]$/.test(username)) return res.sendStatus(404);
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/following`,
    type: "OrderedCollection",
    totalItems: 0,
  };
  res.type("application/activity+json").json(body);
});

app.get("/u/:username/followers", (req, res) => {
  const username = req.params.username;
  const hostname = new URL(CONFIG.origin).hostname;
  if (username.length !== 1) return res.sendStatus(404);
  if (!/^[a-z]$/.test(username)) return res.sendStatus(404);
  const body = {
    "@context": "https://www.w3.org/ns/activitystreams",
    id: `https://${hostname}/u/${username}/followers`,
    type: "OrderedCollection",
    totalItems: 0,
  };
  res.type("application/activity+json").json(body);
});

app.post("/s/:secret/u/:username", async (req, res) => {
  try {
    const username = req.params.username;
    const hostname = new URL(CONFIG.origin).hostname;
    const send = req.body;
    const t = send.type ?? "";
    if (username.length !== 1) return res.sendStatus(404);
    if (!/^[a-z]$/.test(username)) return res.sendStatus(404);
    if (!req.params.secret || req.params.secret === "-") return res.sendStatus(404);
    if (req.params.secret !== process.env.SECRET) return res.sendStatus(404);
    if (new URL(send.id || "about:blank").protocol !== "https:") return res.sendStatus(400);
    const x = await getActivity(username, hostname, send.id);
    if (!x) return res.sendStatus(500);
    const aid = x.id ?? "";
    const atype = x.type ?? "";
    if (aid.length > 1024 || atype.length > 64) return res.sendStatus(400);
    if (t === "follow") {
      await follow(username, hostname, x);
      return res.status(200).end();
    }
    if (t === "undo_follow") {
      await undoFollow(username, hostname, x);
      return res.status(200).end();
    }
    if (t === "like") {
      if (new URL(x.attributedTo || "about:blank").protocol !== "https:") return res.sendStatus(400);
      const y = await getActivity(username, hostname, x.attributedTo);
      if (!y) return res.sendStatus(500);
      await like(username, hostname, x, y);
      return res.status(200).end();
    }
    if (t === "undo_like") {
      if (new URL(x.attributedTo || "about:blank").protocol !== "https:") return res.sendStatus(400);
      const y = await getActivity(username, hostname, x.attributedTo);
      if (!y) return res.sendStatus(500);
      await undoLike(username, hostname, x, y);
      return res.status(200).end();
    }
    console.log(`TYPE ${aid} ${atype}`);
    res.status(200).end();
  } catch (_err) {
    res.sendStatus(500);
  }
});

app.get("/.well-known/nodeinfo", (req, res) => {
  const hostname = new URL(CONFIG.origin).hostname;
  const body = {
    links: [
      {
        rel: "http://nodeinfo.diaspora.software/ns/schema/2.0",
        href: `https://${hostname}/nodeinfo/2.0.json`,
      },
      {
        rel: "http://nodeinfo.diaspora.software/ns/schema/2.1",
        href: `https://${hostname}/nodeinfo/2.1.json`,
      },
    ],
  };
  const headers = {
    "Cache-Control": `public, max-age=${Number(CONFIG.ttl) || 0}, must-revalidate`,
    Vary: "Accept, Accept-Encoding",
  };
  res.set(headers).json(body);
});

app.get("/.well-known/webfinger", (req, res) => {
  let username = "";
  const hostname = new URL(CONFIG.origin).hostname;
  const c = "abcdefghijklmnopqrstuvwxyz";
  const p443 = `https://${hostname}:443/`;
  let resource = req.query.resource;
  let hasResource = false;
  if (resource?.startsWith(p443)) resource = `https://${hostname}/${resource.slice(p443.length)}`;
  for (let i = 0; i < c.length; i++) {
    username = c.charAt(i);
    if (resource === `acct:${username}@${hostname}`) hasResource = true;
    if (resource === `mailto:${username}@${hostname}`) hasResource = true;
    if (resource === `https://${hostname}/@${username}`) hasResource = true;
    if (resource === `https://${hostname}/u/${username}`) hasResource = true;
    if (resource === `https://${hostname}/user/${username}`) hasResource = true;
    if (resource === `https://${hostname}/users/${username}`) hasResource = true;
    if (hasResource) break;
  }
  if (!hasResource) return res.sendStatus(404);
  const body = {
    subject: `acct:${username}@${hostname}`,
    aliases: [
      `mailto:${username}@${hostname}`,
      `https://${hostname}/@${username}`,
      `https://${hostname}/u/${username}`,
      `https://${hostname}/user/${username}`,
      `https://${hostname}/users/${username}`,
    ],
    links: [
      {
        rel: "self",
        type: "application/activity+json",
        href: `https://${hostname}/u/${username}`,
      },
      {
        rel: "http://webfinger.net/rel/avatar",
        type: "image/png",
        href: `https://${hostname}/public/u.png`,
      },
      {
        rel: "http://webfinger.net/rel/profile-page",
        type: "text/plain",
        href: `https://${hostname}/u/${username}`,
      },
    ],
  };
  const headers = {
    "Cache-Control": `public, max-age=${Number(CONFIG.ttl) || 0}, must-revalidate`,
    Vary: "Accept, Accept-Encoding",
  };
  res.set(headers).type("application/jrd+json").json(body);
});

app.get("/@", (_req, res) => res.redirect("/"));
app.get("/u", (_req, res) => res.redirect("/"));
app.get("/user", (_req, res) => res.redirect("/"));
app.get("/users", (_req, res) => res.redirect("/"));

app.get("/users/:username", (req, res) => res.redirect(`/u/${req.params.username}`));
app.get("/user/:username", (req, res) => res.redirect(`/u/${req.params.username}`));
app.get("/@:username", (req, res) => res.redirect(`/u/${req.params.username}`));

app.use((_req, res) => res.sendStatus(404));
app.listen(process.env.PORT || 8080, process.env.HOSTS || "localhost");
