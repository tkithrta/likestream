# Likestream

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io/#https://gitlab.com/tkithrta/likestream)

```shell
$ command -v bash curl docker git openssl sed
$ command -v bash curl docker git openssl sed | wc -l
6
```

```shell
$ curl -fsSL https://gitlab.com/tkithrta/likestream/-/raw/main/install.sh | PORT=8080 bash -s -- https://www.example.com
$ curl https://www.example.com/
<h1>Likestream</h1><!-- https://gitlab.com/tkithrta/likestream -->
```

```shell
$ curl -fsSL https://gitlab.com/tkithrta/likestream/-/raw/main/setup.sh | bash -s -- https://www.example.com
$ docker run -d -v "$PWD/data:/app/data" -p 8080:8080 -e PORT=8080 --env-file=.env --name=psh registry.gitlab.com/tkithrta/likestream/main
$ curl https://www.example.com/
<h1>Likestream</h1><!-- https://gitlab.com/tkithrta/likestream -->
```

SPDX-License-Identifier: MIT
